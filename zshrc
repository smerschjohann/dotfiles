# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-rust \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-bin-gem-node
### End of Zinit's installer chunk

zplugin ice depth=1; zplugin light romkatv/powerlevel10k

zplugin wait lucid for        \
      zpm-zsh/autoenv         \
      MikeDacre/cdbk          \
      ChrisPenner/copy-pasta  \
      Aloxaf/fzf-tab           \
      reegnz/jq-zsh-plugin    \
      Tarrasch/zsh-bd         \
      MikeDacre/cdbk          \
      zpm-zsh/clipboard

zinit wait lucid for \
      atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
      zdharma-continuum/fast-syntax-highlighting \
      blockf \
      zsh-users/zsh-completions \
      atload"!_zsh_autosuggest_start" \
      zsh-users/zsh-autosuggestions 

zinit wait"1" lucid from"gh-r" as"null" for \
      sbin"**/fd"        @sharkdp/fd \
      sbin"**/bat"       @sharkdp/bat \
      sbin"exa* -> exa"  ogham/exa \
      sbin"k3d* -> k3d"  rancher/k3d \
      sbin"k9s* -> k9s"  derailed/k9s \
      sbin"helmfile* -> helmfile" roboll/helmfile \
      sbin"fzf"          junegunn/fzf \
      sbin"sops* -> sops" bpick"*linux*" mozilla/sops \
      sbin"**/lsd"       Peltoche/lsd 

zinit wait"2" lucid from"gh-r" as"null" for \
	sbin"jx"           jenkins-x/jx \
	sbin"yq* -> yq"           mikefarah/yq \
	sbin"gucci* -> gucci"        noqcks/gucci

zinit wait"1" lucid from"gh-r" as"null" for \
     sbin"gcrane;crane" google/go-containerregistry	

zinit from"gh-r" as"null" lucid wait"1" bpick"kubens" bpick"kubectx" sbin"kubens;kubectx" for ahmetb/kubectx 

zinit ice depth'1' as"null" sbin="ranger.py -> ranger"
zinit light ranger/ranger

zinit ice gem'colorls;'
zinit load zdharma-continuum/null
#zinit load gretzky/auto-color-ls

#zinit load hkupty/ssh-agent
#zstyle :omz:plugins:ssh-agent agent-forwarding on

zplugin snippet OMZ::plugins/kubectl/kubectl.plugin.zsh
zplugin snippet OMZ::plugins/oc/oc.plugin.zsh

#bindkey '^[[3~'   delete-char

# vi keybindings
bindkey -v
# basic key bindings (portable, based on zsh article in the Official Arch Wiki)
typeset -A key
key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
bindkey '^R' history-incremental-search-backward
bindkey '^S' history-incremental-search-forward
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       up-line-or-history
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     down-line-or-history
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history


bindkey "^[[1;5D" backward-word # ctrl + arrow left
bindkey "^[[1;5C" forward-word # ctrl + arrow right
bindkey "^[[H" beginning-of-line # home
bindkey "^[[F" end-of-line # end
bindkey "^[[3~" delete-char # end

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data


alias ls='lsd'
alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'

# # # [d] Sort output with directories first
#alias lsd="colorls -A --sort-dirs"
alias lld="ll --sort-dirs"
alias ldd="ld --sort-dirs"
alias lad="la --sort-dirs"
# # # [t] Sort output with recent modified first
alias lst="colorls -A -t"
alias llt="ll -t"
alias ldt="ld -t"
alias lat="la -t"
# # # [g] Add git status of each item in output
alias lsg="colorls -A --git-status"
alias llg="ll --git-status"
alias ldg="ld --git-status"
alias lag="la --git-status"

alias k=kubectl


export PATH=$PATH:~/bin
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte-2.91.sh
fi


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
[[ ! -f ~/.zshrc-local ]] || source ~/.zshrc-local

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
